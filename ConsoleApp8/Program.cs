﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
           System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Title = "Лабораторна робота №7";
            Airplane[] arr;
            arr = ReadAirplaneArray();
            do
            {
                int ch = PrintMenu();
                Console.WriteLine("Натисніть будь-яку клавішу.\n");
                Console.ReadKey();
                Console.Clear();
                switch (ch)
                {
                    case 1:
                        arr = ReadAirplaneArray();
                        break;
                    case 2:
                        PrintAirplanes(arr);
                        break;
                    case 3:
                        Console.WriteLine($"Номер подорожі від 1 до {arr.Length}");
                        int a; YourValue(out a);
                        PrintAirplane(arr[a - 1]);
                        break;
                    case 4:
                        arr = SortAirplanesByDate(arr);
                        PrintAirplanes(arr);
                        break;
                    case 5:
                        arr = SortAirplanesByTotalTime(arr);
                        PrintAirplanes(arr);
                        break;
                    case 6:
                        double min, max;
                        int min_i, max_i;
                        GetAirplaneInfo(arr, out min, out max, out min_i, out max_i);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.ResetColor();
                        Console.WriteLine($"Максимальний час подорожі:{max}\n");
                        PrintAirplane(arr[max_i]);
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("----------------------------------------------------------------------------------------------------");
                        Console.ResetColor();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"Мінімальний час подорожі:{min}\n");
                        Console.ResetColor();
                        PrintAirplane(arr[min_i]);
                        break;
                    default:
                        Console.WriteLine("Введено неправильне значення!");
                        break;
                }
            } while (true);
        }
        static void CheckString(out string n)
        {
            do
            {
                n = Console.ReadLine();
                if (n.Length <= 0)
                {
                    Console.WriteLine("Помилка введення значення! Спробуйте ще раз!");
                }
            } while (n.Length <= 0);
        }
        static void CheckNumber(out int number, int min, int max)
        {
            bool checknumber;
            do
            {
                checknumber = int.TryParse(Console.ReadLine(), out number);
                if (!checknumber || number < min || number > max)
                    Console.WriteLine("Помилка введення значення! Спробуйте ще раз!");
            } while (!checknumber || number < min || number > max);
        }
        static void CheckNumberFloat(out double number)
        {
            bool checknumber;
            do
            {
                checknumber = double.TryParse(Console.ReadLine(), out number);
                if (!checknumber || number <= 0)
                    Console.WriteLine("Помилка введення значення! Спробуйте ще раз!");
            } while (!checknumber || number <= 0);
        }
        static Airplane[] ReadAirplaneArray()
        {
            Console.Write("Доброго дня!\nВведіть кількість рейсів ->");
            int n;
            YourValue(out n);
            Airplane[] arr = new Airplane[n];
            for (int i = 0; i < n; i++)
            {
                Airplane airplane = new Airplane();
                Console.WriteLine($"[{i + 1}]");
                Console.Write("Місто відправлення: ");
                string startcity; CheckString(out startcity); airplane.SetStartCity(startcity);
                Console.Write("\t Рік: ");
                int year; CheckNumber(out year, 1960, 2021); airplane.SetYear(year);
                Console.Write("\t Місяць: ");
                int month; CheckNumber(out month, 1, 12); airplane.SetMonth(month);
                Console.Write("\t День: ");
                int day; CheckNumber(out day, 1, 31); airplane.SetDay(day);
                Console.Write("\t Години: ");
                int hours; CheckNumber(out hours, 0, 24); airplane.SetHours(hours);
                Console.Write("\t Хвилини: ");
                int minutes; CheckNumber(out minutes, 0, 59); airplane.SetMinutes(minutes);
                Console.Write("Місто прибуття: ");
                string finishcity; CheckString(out finishcity); airplane.SetFinishCity(finishcity);
                Console.Write("\t Рік: ");
                int yearfinish; CheckNumber(out yearfinish, 1960, 2021); airplane.SetYearFinish(yearfinish);
                Console.Write("\t Місяць: ");
                int monthfinish; CheckNumber(out monthfinish, 1, 12); airplane.SetMonthFinish(monthfinish);
                Console.Write("\t День: ");
                int dayfinish; CheckNumber(out dayfinish, 1, 31); airplane.SetDayFinish(dayfinish);
                Console.Write("\t Години: ");
                int hoursfinish; CheckNumber(out hoursfinish, 0, 24); airplane.SetHoursFinish(hoursfinish);
                Console.Write("\t Хвилини: ");
                int minutesfinish; CheckNumber(out minutesfinish, 0, 60); airplane.SetMinutesFinish(minutesfinish);
                Console.WriteLine("Відстань:");
                Console.WriteLine("\t1-Метри");
                Console.WriteLine("\t2-Кілометри");
                Console.WriteLine("\t3-Милі");
                int k; YourValue(out k);
                Console.Write("Введіть відстань: ");
                double distance; CheckNumberFloat(out distance);
                switch (k)
                {
                    case 1:
                        airplane.Metres = distance;
                        airplane.Killometres = distance/1000;
                        airplane.Miles = distance * 0.00062;
                        break;
                    case 2:
                        airplane.Metres = distance * 1000;
                        airplane.Killometres = distance;
                        airplane.Miles = distance * 0.62;
                        break;
                    case 3:
                        airplane.Metres = distance * 1609.34;
                        airplane.Killometres = distance * 1.60934;
                        airplane.Miles = distance;
                        break;
                }
                arr[i] = new Airplane(airplane.GetStartCity(), airplane.GetFinishCity(), airplane.GetYear(), airplane.GetMonth(), airplane.GetDay(), airplane.GetHours(), airplane.GetMinutes(), airplane.GetYearFinish(), airplane.GetMonthFinish(), airplane.GetDayFinish(), airplane.GetHoursFinish(), airplane.GetMinutesFinish(), airplane.Metres, airplane.Killometres, airplane.Miles);
            }
            return arr;
        }
        static void PrintAirplane(Airplane a)
        {
            Console.WriteLine($"\t{a.GetStartCity()} - {a.GetFinishCity()}\n");
            Console.WriteLine($"Час відправлення: { a.GetDay()}.{ a.GetMonth()}.{ a.GetYear()} { a.GetHours()}:{ a.GetMinutes()}\n");
            Console.WriteLine($"Час прибуття: { a.GetDayFinish()}.{ a.GetMonthFinish()}.{ a.GetYearFinish()} { a.GetHoursFinish()}:{ a.GetMinutesFinish()}\n");
            Console.WriteLine($"\t Загальний час подорожі: {a.GetTotalTime(a.GetYear(), a.GetMonth(), a.GetDay(), a.GetHours(), a.GetMinutes(), a.GetYearFinish(), a.GetMonthFinish(), a.GetDayFinish(), a.GetHoursFinish(), a.GetMinutesFinish())} хвилин");
            if (a.IsArrivingToday(a.GetYear(), a.GetMonth(), a.GetDay(), a.GetYearFinish(), a.GetMonthFinish(), a.GetDayFinish()))
            {
                Console.WriteLine($"Відправлення та прибуття в один день: так\n");
            }
            else
            {
                Console.WriteLine($"Відправлення та прибуття в один день: ні\n");
            }
            Console.WriteLine($"Відстань: {a.Metres:F2} m   {a.Killometres:F2} km   {a.Miles:F2} mi");
        }
        static void PrintAirplanes(Airplane[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("---------------------------------------------------------------------------------------------------");

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\tРейс №[{i + 1}]");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("---------------------------------------------------------------------------------------------------");

                Console.ResetColor();
                PrintAirplane(arr[i]);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("---------------------------------------------------------------------------------------------------");

                Console.ResetColor();
            }
        }
        static void GetAirplaneInfo(Airplane[] arr, out double Min, out double Max, out int min_i, out int max_i)
        {
            Min = arr[0].GetTotalTime(arr[0].GetYear(), arr[0].GetMonth(), arr[0].GetDay(), arr[0].GetHours(), arr[0].GetMinutes(), arr[0].GetYearFinish(), arr[0].GetMonthFinish(), arr[0].GetDayFinish(), arr[0].GetHoursFinish(), arr[0].GetMinutesFinish());
            Max = arr[0].GetTotalTime(arr[0].GetYear(), arr[0].GetMonth(), arr[0].GetDay(), arr[0].GetHours(), arr[0].GetMinutes(), arr[0].GetYearFinish(), arr[0].GetMonthFinish(), arr[0].GetDayFinish(), arr[0].GetHoursFinish(), arr[0].GetMinutesFinish());
            min_i = 0;
            max_i = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                double tmp = arr[i].GetTotalTime(arr[i].GetYear(), arr[i].GetMonth(), arr[i].GetDay(), arr[i].GetHours(), arr[i].GetMinutes(), arr[i].GetYearFinish(), arr[i].GetMonthFinish(), arr[i].GetDayFinish(), arr[i].GetHoursFinish(), arr[i].GetMinutesFinish());
                if (tmp < Min)
                {
                    Min = tmp;
                    min_i = i;
                }
                else if (tmp > Max)
                {
                    Max = tmp;
                    max_i = i;
                }
            }
        }
        static int ComparDate(Airplane x, Airplane y)
        {
            double dateX = x.GetTime(x.GetYear(), x.GetMonth(), x.GetDay(), x.GetHours(), x.GetMinutes());
            double dateY = y.GetTime(y.GetYear(), y.GetMonth(), y.GetDay(), y.GetHours(), y.GetMinutes());
            if (dateX < dateY)
                return 1;
            if (dateX > dateY)
                return -1;
            return 0;
        }
        static Airplane[] SortAirplanesByDate(Airplane[] a)
        {
            Array.Sort(a, ComparDate);
            return a;
        }
        static int ComparTime(Airplane x, Airplane y)
        {
            double timeX = x.GetTotalTime(x.GetYear(), x.GetMonth(), x.GetDay(), x.GetHours(), x.GetMinutes(), x.GetYearFinish(), x.GetMonthFinish(), x.GetDayFinish(), x.GetHoursFinish(), x.GetMinutesFinish());
            double timeY = y.GetTotalTime(y.GetYear(), y.GetMonth(), y.GetDay(), y.GetHours(), y.GetMinutes(), y.GetYearFinish(), y.GetMonthFinish(), y.GetDayFinish(), y.GetHoursFinish(), y.GetMinutesFinish());
            if (timeX > timeY)
                return 1;
            else if (timeX < timeY)
                return -1;
            else
                return 0;
        }
        static Airplane[] SortAirplanesByTotalTime(Airplane[] a)
        {
            Array.Sort(a, ComparTime);
            return a;
        }
        static void YourValue(out int n)
        {
            bool number;
            do
            {
                number = int.TryParse(Console.ReadLine(), out n);
                if (!number || n <= 0)
                {
                    Console.WriteLine("Помилка введення значення!");
                }
            } while (!number);
        }
        static int PrintMenu()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\tМеню вибору:");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("1 - Ввести масив рейсів");
            Console.WriteLine("2 - Показати усі рейси");
            Console.WriteLine("3 - Вивести певний рейс");
            Console.WriteLine("4 - Сортувати масив за спаданням дати відправлення");
            Console.WriteLine("5 - Сортувати масив за зростанням часу подорожі");
            Console.WriteLine("6 - Найбільший та найменший час подорожі");
            Console.ResetColor();
            int a; YourValue(out a);
            return a;
        }
    }
}
