﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlazedForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox_material.SelectedIndex = 0;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }
        private void button_result_Click(object sender, EventArgs e)
        {
            double width, heigth;
            bool check_num;
            check_num = double.TryParse(textBox_width.Text, out width);
            if (!check_num || width <= 0)
            {
                MessageBox.Show("Помилка введення значення!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            check_num = double.TryParse(textBox_height.Text, out heigth);
            if (!check_num || heigth <= 0)
            {
                MessageBox.Show("Помилка введення значення!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            double rezult = 0;
            switch (comboBox_material.SelectedIndex)
            {
                case 0:
                    if (radioButton_one.Checked)
                    {
                        rezult = width * heigth * 0.25;
                    }
                    else
                    {
                        rezult = width * heigth * 0.30;
                    }
                    break;
                case 1:
                    if (radioButton_one.Checked)
                    {
                        rezult = width * heigth * 0.05;
                    }
                    else
                    {
                        rezult = width * heigth * 0.10;
                    }
                    break;
                case 2:
                    if (radioButton_one.Checked)
                    {
                        rezult = width * heigth * 0.15;
                    }
                    else
                    {
                        rezult = width * heigth * 0.20;
                    }
                    break;
            }
            if (checkBox_pidvikonnya.Checked)
            {
                rezult += 35;
            }
            label_rezult.Text = rezult.ToString("F2");
        }
    }
}
