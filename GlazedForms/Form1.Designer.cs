﻿
namespace GlazedForms
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label price;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.sizeofwindow = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_width = new System.Windows.Forms.TextBox();
            this.textBox_height = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox_material = new System.Windows.Forms.ComboBox();
            this.sklopaket = new System.Windows.Forms.Label();
            this.radioButton_one = new System.Windows.Forms.RadioButton();
            this.radioButton_second = new System.Windows.Forms.RadioButton();
            this.checkBox_pidvikonnya = new System.Windows.Forms.CheckBox();
            this.button_result = new System.Windows.Forms.Button();
            this.label_rezult = new System.Windows.Forms.Label();
            price = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // price
            // 
            price.AutoSize = true;
            price.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            price.ForeColor = System.Drawing.Color.Black;
            price.Location = new System.Drawing.Point(12, 256);
            price.Name = "price";
            price.Size = new System.Drawing.Size(104, 25);
            price.TabIndex = 7;
            price.Text = "Вартість:";
            // 
            // sizeofwindow
            // 
            this.sizeofwindow.AutoSize = true;
            this.sizeofwindow.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sizeofwindow.Location = new System.Drawing.Point(13, 24);
            this.sizeofwindow.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.sizeofwindow.Name = "sizeofwindow";
            this.sizeofwindow.Size = new System.Drawing.Size(131, 22);
            this.sizeofwindow.TabIndex = 0;
            this.sizeofwindow.Text = "Розміри вікна";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ширина (см):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Висота (см):";
            // 
            // textBox_width
            // 
            this.textBox_width.Location = new System.Drawing.Point(184, 70);
            this.textBox_width.Name = "textBox_width";
            this.textBox_width.Size = new System.Drawing.Size(165, 29);
            this.textBox_width.TabIndex = 3;
            // 
            // textBox_height
            // 
            this.textBox_height.Location = new System.Drawing.Point(184, 126);
            this.textBox_height.Name = "textBox_height";
            this.textBox_height.Size = new System.Drawing.Size(165, 29);
            this.textBox_height.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "Матеріал:";
            // 
            // comboBox_material
            // 
            this.comboBox_material.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_material.FormattingEnabled = true;
            this.comboBox_material.Items.AddRange(new object[] {
            "Дерево",
            "Метал",
            "Металопластик"});
            this.comboBox_material.Location = new System.Drawing.Point(184, 178);
            this.comboBox_material.Name = "comboBox_material";
            this.comboBox_material.Size = new System.Drawing.Size(165, 28);
            this.comboBox_material.TabIndex = 6;
            // 
            // sklopaket
            // 
            this.sklopaket.AutoSize = true;
            this.sklopaket.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sklopaket.Location = new System.Drawing.Point(408, 24);
            this.sklopaket.Name = "sklopaket";
            this.sklopaket.Size = new System.Drawing.Size(106, 22);
            this.sklopaket.TabIndex = 8;
            this.sklopaket.Text = "Склопакет";
            // 
            // radioButton_one
            // 
            this.radioButton_one.AutoSize = true;
            this.radioButton_one.Checked = true;
            this.radioButton_one.Location = new System.Drawing.Point(413, 69);
            this.radioButton_one.Name = "radioButton_one";
            this.radioButton_one.Size = new System.Drawing.Size(149, 25);
            this.radioButton_one.TabIndex = 9;
            this.radioButton_one.TabStop = true;
            this.radioButton_one.Text = "Однокамерний";
            this.radioButton_one.UseVisualStyleBackColor = true;
            // 
            // radioButton_second
            // 
            this.radioButton_second.AutoSize = true;
            this.radioButton_second.Location = new System.Drawing.Point(413, 106);
            this.radioButton_second.Name = "radioButton_second";
            this.radioButton_second.Size = new System.Drawing.Size(137, 25);
            this.radioButton_second.TabIndex = 10;
            this.radioButton_second.Text = "Двокамерний";
            this.radioButton_second.UseVisualStyleBackColor = true;
            // 
            // checkBox_pidvikonnya
            // 
            this.checkBox_pidvikonnya.AutoSize = true;
            this.checkBox_pidvikonnya.Checked = true;
            this.checkBox_pidvikonnya.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_pidvikonnya.Location = new System.Drawing.Point(413, 181);
            this.checkBox_pidvikonnya.Name = "checkBox_pidvikonnya";
            this.checkBox_pidvikonnya.Size = new System.Drawing.Size(116, 25);
            this.checkBox_pidvikonnya.TabIndex = 11;
            this.checkBox_pidvikonnya.Text = "Підвіконня";
            this.checkBox_pidvikonnya.UseVisualStyleBackColor = true;
            // 
            // button_result
            // 
            this.button_result.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_result.Location = new System.Drawing.Point(413, 249);
            this.button_result.Name = "button_result";
            this.button_result.Size = new System.Drawing.Size(191, 49);
            this.button_result.TabIndex = 12;
            this.button_result.Text = "Розрахувати";
            this.button_result.UseVisualStyleBackColor = true;
            this.button_result.Click += new System.EventHandler(this.button_result_Click);
            // 
            // label_rezult
            // 
            this.label_rezult.AutoSize = true;
            this.label_rezult.Location = new System.Drawing.Point(179, 256);
            this.label_rezult.Name = "label_rezult";
            this.label_rezult.Size = new System.Drawing.Size(0, 21);
            this.label_rezult.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RosyBrown;
            this.ClientSize = new System.Drawing.Size(634, 379);
            this.Controls.Add(this.label_rezult);
            this.Controls.Add(this.button_result);
            this.Controls.Add(this.checkBox_pidvikonnya);
            this.Controls.Add(this.radioButton_second);
            this.Controls.Add(this.radioButton_one);
            this.Controls.Add(this.sklopaket);
            this.Controls.Add(price);
            this.Controls.Add(this.comboBox_material);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_height);
            this.Controls.Add(this.textBox_width);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sizeofwindow);
            this.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximumSize = new System.Drawing.Size(650, 418);
            this.MinimumSize = new System.Drawing.Size(650, 418);
            this.Name = "Form1";
            this.Text = " ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label sizeofwindow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_width;
        private System.Windows.Forms.TextBox textBox_height;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox_material;
        private System.Windows.Forms.Label sklopaket;
        private System.Windows.Forms.RadioButton radioButton_one;
        private System.Windows.Forms.RadioButton radioButton_second;
        private System.Windows.Forms.CheckBox checkBox_pidvikonnya;
        private System.Windows.Forms.Button button_result;
        private System.Windows.Forms.Label label_rezult;
    }
}

