﻿
namespace TravelForm
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.country = new System.Windows.Forms.Label();
            this.comboBox_country = new System.Windows.Forms.ComboBox();
            this.day = new System.Windows.Forms.Label();
            this.tickets = new System.Windows.Forms.Label();
            this.numericUpDown_days = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_tickets = new System.Windows.Forms.NumericUpDown();
            this.label_seasons = new System.Windows.Forms.Label();
            this.radioButton_summer = new System.Windows.Forms.RadioButton();
            this.radioButton_winter = new System.Windows.Forms.RadioButton();
            this.guide = new System.Windows.Forms.CheckBox();
            this.button_result = new System.Windows.Forms.Button();
            this.result = new System.Windows.Forms.Label();
            this.labelforrezult = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_days)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_tickets)).BeginInit();
            this.SuspendLayout();
            // 
            // country
            // 
            this.country.AutoSize = true;
            this.country.Location = new System.Drawing.Point(20, 26);
            this.country.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.country.Name = "country";
            this.country.Size = new System.Drawing.Size(69, 21);
            this.country.TabIndex = 0;
            this.country.Text = "Країна:";
            // 
            // comboBox_country
            // 
            this.comboBox_country.FormattingEnabled = true;
            this.comboBox_country.Items.AddRange(new object[] {
            "Болгарія",
            "Німеччина",
            "Польща"});
            this.comboBox_country.Location = new System.Drawing.Point(220, 19);
            this.comboBox_country.Name = "comboBox_country";
            this.comboBox_country.Size = new System.Drawing.Size(147, 28);
            this.comboBox_country.TabIndex = 1;
            // 
            // day
            // 
            this.day.AutoSize = true;
            this.day.Location = new System.Drawing.Point(20, 76);
            this.day.Name = "day";
            this.day.Size = new System.Drawing.Size(126, 21);
            this.day.TabIndex = 2;
            this.day.Text = "Кількість днів:";
            // 
            // tickets
            // 
            this.tickets.AutoSize = true;
            this.tickets.Location = new System.Drawing.Point(20, 121);
            this.tickets.Name = "tickets";
            this.tickets.Size = new System.Drawing.Size(151, 21);
            this.tickets.TabIndex = 3;
            this.tickets.Text = "Кількість квитків:";
            // 
            // numericUpDown_days
            // 
            this.numericUpDown_days.Location = new System.Drawing.Point(220, 69);
            this.numericUpDown_days.Name = "numericUpDown_days";
            this.numericUpDown_days.Size = new System.Drawing.Size(147, 29);
            this.numericUpDown_days.TabIndex = 4;
            this.numericUpDown_days.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numericUpDown_tickets
            // 
            this.numericUpDown_tickets.Location = new System.Drawing.Point(220, 119);
            this.numericUpDown_tickets.Name = "numericUpDown_tickets";
            this.numericUpDown_tickets.Size = new System.Drawing.Size(147, 29);
            this.numericUpDown_tickets.TabIndex = 5;
            this.numericUpDown_tickets.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label_seasons
            // 
            this.label_seasons.AutoSize = true;
            this.label_seasons.Location = new System.Drawing.Point(20, 171);
            this.label_seasons.Name = "label_seasons";
            this.label_seasons.Size = new System.Drawing.Size(98, 21);
            this.label_seasons.TabIndex = 6;
            this.label_seasons.Text = "Пора року:";
            // 
            // radioButton_summer
            // 
            this.radioButton_summer.AutoSize = true;
            this.radioButton_summer.Checked = true;
            this.radioButton_summer.Location = new System.Drawing.Point(176, 169);
            this.radioButton_summer.Name = "radioButton_summer";
            this.radioButton_summer.Size = new System.Drawing.Size(63, 25);
            this.radioButton_summer.TabIndex = 7;
            this.radioButton_summer.TabStop = true;
            this.radioButton_summer.Text = "Літо";
            this.radioButton_summer.UseVisualStyleBackColor = true;
            // 
            // radioButton_winter
            // 
            this.radioButton_winter.AutoSize = true;
            this.radioButton_winter.Location = new System.Drawing.Point(290, 169);
            this.radioButton_winter.Name = "radioButton_winter";
            this.radioButton_winter.Size = new System.Drawing.Size(68, 25);
            this.radioButton_winter.TabIndex = 8;
            this.radioButton_winter.Text = "Зима";
            this.radioButton_winter.UseVisualStyleBackColor = true;
            // 
            // guide
            // 
            this.guide.AutoSize = true;
            this.guide.Checked = true;
            this.guide.CheckState = System.Windows.Forms.CheckState.Checked;
            this.guide.Location = new System.Drawing.Point(146, 220);
            this.guide.Name = "guide";
            this.guide.Size = new System.Drawing.Size(180, 25);
            this.guide.TabIndex = 9;
            this.guide.Text = "Індивідуальний гід";
            this.guide.UseVisualStyleBackColor = true;
            // 
            // button_result
            // 
            this.button_result.ForeColor = System.Drawing.Color.DarkGreen;
            this.button_result.Location = new System.Drawing.Point(443, 196);
            this.button_result.Name = "button_result";
            this.button_result.Size = new System.Drawing.Size(200, 55);
            this.button_result.TabIndex = 10;
            this.button_result.Text = "Обрахувати";
            this.button_result.UseVisualStyleBackColor = true;
            this.button_result.Click += new System.EventHandler(this.button_result_Click);
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.Location = new System.Drawing.Point(20, 285);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(90, 21);
            this.result.TabIndex = 11;
            this.result.Text = "Результат:";
            // 
            // labelforrezult
            // 
            this.labelforrezult.AutoSize = true;
            this.labelforrezult.Location = new System.Drawing.Point(367, 285);
            this.labelforrezult.Name = "labelforrezult";
            this.labelforrezult.Size = new System.Drawing.Size(0, 21);
            this.labelforrezult.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(684, 411);
            this.Controls.Add(this.labelforrezult);
            this.Controls.Add(this.result);
            this.Controls.Add(this.button_result);
            this.Controls.Add(this.guide);
            this.Controls.Add(this.radioButton_winter);
            this.Controls.Add(this.radioButton_summer);
            this.Controls.Add(this.label_seasons);
            this.Controls.Add(this.numericUpDown_tickets);
            this.Controls.Add(this.numericUpDown_days);
            this.Controls.Add(this.tickets);
            this.Controls.Add(this.day);
            this.Controls.Add(this.comboBox_country);
            this.Controls.Add(this.country);
            this.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximumSize = new System.Drawing.Size(700, 450);
            this.MinimumSize = new System.Drawing.Size(700, 450);
            this.Name = "Form1";
            this.Text = "Лабораторна робота №7";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_days)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_tickets)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label country;
        private System.Windows.Forms.ComboBox comboBox_country;
        private System.Windows.Forms.Label day;
        private System.Windows.Forms.Label tickets;
        private System.Windows.Forms.NumericUpDown numericUpDown_days;
        private System.Windows.Forms.NumericUpDown numericUpDown_tickets;
        private System.Windows.Forms.Label label_seasons;
        private System.Windows.Forms.RadioButton radioButton_summer;
        private System.Windows.Forms.RadioButton radioButton_winter;
        private System.Windows.Forms.CheckBox guide;
        private System.Windows.Forms.Button button_result;
        private System.Windows.Forms.Label result;
        private System.Windows.Forms.Label labelforrezult;
    }
}

