﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox_country.SelectedIndex = 0;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }
        private void button_result_Click(object sender, EventArgs e)
        {
            int rezult = 0;
            int days = Convert.ToInt32(numericUpDown_days.Value);
            int tickets = Convert.ToInt32(numericUpDown_tickets.Value);
            switch (comboBox_country.SelectedIndex)
            {
                case 0:
                    if (radioButton_summer.Checked)
                    {
                        rezult = days * 100 * tickets;
                    }
                    else
                    {
                        rezult = days * 150 * tickets;
                    }
                    break;
                case 1:
                    if (radioButton_summer.Checked)
                    {
                        rezult = days * 160 * tickets;
                    }
                    else
                    {
                        rezult = days * 200 * tickets;
                    }
                    break;
                case 2:
                    if (radioButton_summer.Checked)
                    {
                        rezult = days * 120 * tickets;
                    }
                    else
                    {
                        rezult = days * 180 * tickets;
                    }
                    break;
            }
            if (guide.Checked)
            {
                rezult += 50 * days;
            }
            labelforrezult.Text = "$" + rezult.ToString("F2");
        }
    }
}
