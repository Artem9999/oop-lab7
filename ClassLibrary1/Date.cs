﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Date
    {
        protected int Year;
        protected int Month;
        protected int Day;
        protected int Hours;
        protected int Minutes;
        public Date()
        {
        }
        public Date(int year, int month, int day, int hours, int minutes)
        {
            if (year > 0)
                Year = year;
            if (month > 0)
                Month = month;
            if (day > 0)
                Day = day;
            if (hours > 0)
                Hours = hours;
            if (minutes > 0)
                Minutes = minutes;
        }
        public Date(int year, int month, int day)
        {
            if (year > 0)
                Year = year;
            if (month > 0)
                Month = month;
            if (day > 0)
                Day = day;
        }
        public Date(Date obj)
        {
            Year = obj.Year;
            Month = obj.Month;
            Day = obj.Day;
            Hours = obj.Hours;
            Minutes = obj.Minutes;
        }

        public void SetYear(int year)
        {
            Year = year;
        }
        public int GetYear()
        {
            return Year;
        }

        public void SetMonth(int month)
        {
            Month = month;
        }
        public int GetMonth()
        {
            return Month;
        }

        public void SetDay(int day)
        {
            Day = day;
        }
        public int GetDay()
        {
            return Day;
        }

        public void SetHours(int hours)
        {
            Hours = hours;
        }
        public int GetHours()
        {
            return Hours;
        }

        public void SetMinutes(int minutes)
        {
            Minutes = minutes;
        }
        public int GetMinutes()
        {
            return Minutes;
        }
    }
}
